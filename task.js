obj ={
    one: 1, two: 2
}
var func_1 = function(firstName, lastName){
    console.log('firstName:', arguments[0], ',lastName:', arguments[1])
}
setTimeout(()=>{console.log('Обработка запроса завершена!')}, 2000)
function func_2(get){
    return get.one + get.two
}
func_1('Anton', 'Morozov')
console.log('Результат сложения:', func_2(obj))